defmodule CarRental.Tasks.TrustScoreUpdater do
  alias CarRental.Clients
  alias CarRental.TrustScore

  @chunk_size 100
  @api_rate_limit 6000

  @spec perform() :: :ok
  def perform do
    with {:ok, clients} <- CarRental.Clients.list_clients() do
      clients
      |> batch_clients()
      |> Enum.each(&update_trust_scores/1)
    end
  end

  defp batch_clients(clients) do
    clients
    |> Enum.map(&map_client/1)
    |> Enum.chunk_every(@chunk_size)
  end

  defp map_client(%Clients.Client{} = client) do
    %{
      client_id: client.id,
      age: client.age,
      license_number: client.license_number,
      rentals_count: length(client.rental_history)
    }
  end

  defp update_trust_scores(batch) do
    with :ok <- :timer.sleep(@api_rate_limit) do
      TrustScore.calculate_score(%TrustScore.Params{
        clients: batch
      })
      |> Enum.each(&save_score/1)
    end
  end

  defp save_score(%TrustScore.Response{} = score) do
    CarRental.Clients.save_score_for_client(%CarRental.Clients.Params{
      client_id: score.id,
      score: score.score
    })
  end
end
