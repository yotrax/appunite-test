import Config

config :car_rental, CarRental.Scheduler,
  jobs: [
    {"@weekly", {CarRental.Tasks.TrustScoreUpdater, :perform, []}}
  ]
